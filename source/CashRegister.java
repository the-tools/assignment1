import java.util.Scanner;
import java.util.LinkedList;
public class CashRegister
{

  // Function to create linked lists for storing purchase data
  public static LinkedList<String> createList(LinkedList<String> name, LinkedList<Double> cost){
    LinkedList<String> newList = new LinkedList<String>();
    int count = 0;
    
    for(int i=0; i<name.size(); i++){
    String tempString = name.get(i) + cost.get(i);
    String nameOfItem = name.get(i);
    double costOfItem = cost.get(i);

        for(int k=0; k<name.size(); k++){
        String tempString2 = name.get(k) + cost.get(k);
    
            if(tempString.equals(tempString2)){
                count++;
            }
        }

    	String check = nameOfItem + " x" + count + " -$" + costOfItem*count;
    	boolean statement = false;
    	for(int j=0; j<newList.size(); j++){
    		if(check.equals(newList.get(j))){
    			statement = true;
  		}
  	}
        if(statement == false){
            newList.add(nameOfItem + " x" + count + " -$" + costOfItem*count);
        }
        count = 0;
    }
    return newList;  
  }

  public static void main(String[] args){

    // Variable Declarations
    String s, choice;
    double c;
    double cost;
    double tender;
    double change;
    double balance;
    LinkedList<String> item = new LinkedList<String>();
    LinkedList<Double> costItem = new LinkedList<Double>();
    
    String discount = "";
    
    Scanner in = new Scanner(System.in);
    
    System.out.println(":::::::Welcome to the Cash Register Program:::::::");
    
    System.out.print("Please enter cash register's float: $");

    // Loop for ensuring value is valid
    while(!in.hasNextDouble()){
        s = in.next();
        System.out.println("Error: A decimal number must be entered.");
        System.out.print("Please enter cash register's float: $");
    }
    balance = Math.round(in.nextDouble() * 100.0)/100.0;
    in.nextLine();
    
    System.out.print("Would you like to process a transaction? (p)\n");
    System.out.print("Or would you like to quit? (q): ");
    choice = in.nextLine();
    
    // Validating valid input
    while(!choice.equals("p") && !choice.equals("q")){
        System.out.println("Error: Invalid choice.");
        System.out.print("Would you like to process a transaction? (p)\n");
        System.out.print("Or would you like to quit? (q): ");
        choice = in.nextLine();
    }
    
    // For p (process) a loop is used to prompt user for data from customer and sale
    while(choice.equals("p")){
        System.out.print("Please enter the item's name: ");
        s = in.nextLine();
    
        System.out.print("Please enter the item's cost: $");
        while(!in.hasNextDouble()){
            s = in.next();
            System.out.println("Error: A decimal number must be entered.");
            System.out.print("Please enter the item's cost: $");
        }
        c = Math.round(in.nextDouble() * 100.0)/100.0;
        in.nextLine();
    
        // Creates a new transaction with name and price then adds to linked lists
        Transaction trans = new Transaction(s, c);
        item.add(trans.getName());
        costItem.add(trans.getCost());
        cost = trans.getCost();	
    
        // Prompts user for more items in transaction
        System.out.print("Would you like to add more items? (y/n)\n");
        choice = in.nextLine();
        while(!choice.equals("y") && !choice.equals("n")){
            System.out.println("Error: Invalid choice.");
            System.out.print("Would you like to add more items? (y/n)\n");
            choice = in.nextLine();
        }
    
    	while(choice.equals("y")){
    
            System.out.print("Please enter the item's name: ");
            s = in.nextLine();
            System.out.print("Please enter the item's cost: $");

            while(!in.hasNextDouble()){
                s = in.next();
                System.out.println("Error: A decimal number must be entered.");
                System.out.print("Please enter the item's cost: $");
            }
            c = Math.round(in.nextDouble() * 100.0)/100.0;
            in.nextLine();
    
            Transaction additional = new Transaction(s, c);
            item.add(additional.getName());
            costItem.add(additional.getCost());
            cost = cost + additional.getCost();
            System.out.print("Would you like to add more items? (y/n)\n");
            choice = in.nextLine();

            while(!choice.equals("y") && !choice.equals("n")){
                System.out.println("Error: Invalid choice.");
                System.out.print("Would you like to add more items? (y/n)\n");
                choice = in.nextLine();
            }
        }
    
        // Prompts user for use of a coupon if required
        System.out.println("Would you like to apply a coupon? (y/n)");
        choice = in.nextLine();
        while(!choice.equals("y") && !choice.equals("n")){
            System.out.println("Error: Invalid choice.");
            System.out.println("Would you like to apply a coupon? (y/n)");
            choice = in.nextLine();
        }
    
	// On yes, display possible coupons and applies selected coupon (if any)
        if(choice.equals("y")){
            System.out.println("Please select the discount applicable.");
            System.out.println("1 - 5% off");
            System.out.println("2 - 10% off");
            System.out.println("3 - 15% off");
            System.out.println("4 - 20% off");
            System.out.println("5 - 25% off");
            System.out.println("c - cancel");
            System.out.print("Enter Selection: ");
            choice = in.nextLine();

	    // Validates user input for choices
            while(!choice.equals("1") && !choice.equals("2") && !choice.equals("3") && !choice.equals("4") && !choice.equals("5") && !choice.equals("c")){
                System.out.println("Error: Invalid choice.");
                System.out.println("Please select the discount applicable.");
                System.out.println("1 - 5% off");
                System.out.println("2 - 10% off");
                System.out.println("3 - 15% off");
                System.out.println("4 - 20% off");
                System.out.println("5 - 25% off");
                System.out.println("c - cancel");
                System.out.print("Enter Selection: ");
                choice = in.nextLine();
            }
    
            // Applies chosen coupon (or none)
            if(choice.equals("1")) {
            cost = Math.round((cost - (cost/100.0 * 5)) * 100.0)/100.0;
            System.out.println("A 5% discount was applied. The total cost is now $" + cost);
            discount = "A 5% discount was applied";
            }
            else if(choice.equals("2")) {
                cost = Math.round((cost - (cost/100.0 * 10)) * 100.0)/100.0;
                System.out.println("A 10% discount was applied. The total cost is now $" + cost);
                discount = "A 10% discount was applied";
            }
            else if(choice.equals("3")) {
                cost = Math.round((cost - (cost/100.0 * 15)) * 100.0)/100.0;
                System.out.println("A 15% discount was applied. The total cost is now $" + cost);
                discount = "A 15% discount was applied";
            }
            else if(choice.equals("4")) {
                cost = Math.round((cost - (cost/100.0 * 20)) * 100.0)/100.0;
                System.out.println("A 20% discount was applied. The total cost is now $" + cost);
                discount = "A 20% discount was applied";
            }
            else if(choice.equals("5")) {
                cost = Math.round((cost - (cost/100.0 * 25)) * 100.0)/100.0;
                System.out.println("A 25% discount was applied. The total cost is now $" + cost);
                discount = "A 25% discount was applied";
            }
            else {
                System.out.println("No discount was applied.");
            }
    
        }
        System.out.print("Please enter the cash amount tendered: $");

	// Loop until user inputs valid value
        while(!in.hasNextDouble()){
            s = in.next();
            System.out.println("Error: A decimal number must be entered.");
            System.out.print("Please enter the cash amount tendered: $");
        }
        tender = Math.round(in.nextDouble() * 100.0)/100.0;
        in.nextLine();
        change = Math.round((tender - cost) * 100.0)/100.0;
    	
	// Loops for customers to continue paying until total amount is payed
        while(change < 0){
            System.out.println("The customer must pay the remaining: $" + (cost - tender));
            System.out.print("Please enter additional cash amount tendered: $");

            while(!in.hasNextDouble()){
                s = in.next();
                System.out.println("Error: A decimal number must be entered.");
                System.out.print("Please enter the item's cost: $");
            }
            tender += Math.round(in.nextDouble() * 100.0)/100.0;
            in.nextLine();
            change = Math.round((tender - cost) * 100.0)/100.0;
        }
    
        System.out.println("Amount of change required: $" + change);
        balance = balance + tender - (tender - cost);
    
        System.out.print("\nWould you like a receipt?(y/n)");
        choice = in.nextLine();
        while(!choice.equals("y") && !choice.equals("n")){
            System.out.println("Error: Invalid choice.");
            System.out.print("\nWould you like a receipt?(y/n)");
            choice = in.nextLine();
        }

	// Prints a receipt for the customer if requested by looping through the linked lists
        if(choice.equals("y")){
    
            System.out.println("\nReceipt: ");
            LinkedList receiptList = createList(item, costItem);
            for(int i=0; i<receiptList.size(); i++){
                System.out.println(receiptList.get(i));  
            }

            if(!discount.equals("")){
                System.out.println(discount);
                discount = "";
            }
    
            System.out.print("Total - $" + cost + "\n");
            System.out.print("Tendered - $" + tender + "\n");
            System.out.print("Change - $" + change + "\n");
        }
    
	// Begins loop again if user requires, or quits if the user is finished
        System.out.print("\nWould you like to process another transaction? (p)\n");
        System.out.print("Or would you like to quit? (q): ");
        choice = in.nextLine();

        while(!choice.equals("p") && !choice.equals("q")){
            System.out.println("Error: Invalid choice.");
            System.out.print("\nWould you like to process another transaction? (p)\n");
            System.out.print("Or would you like to quit? (q): ");
            choice = in.nextLine();
        }
     }

    // Prints the final balance of the cash register after user is finished    
    System.out.println("Balance of the Cash Register: $" + balance);
  }
}
    