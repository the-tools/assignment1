CashRegister.java changes

Normalized code style during code review.
Indented code during code review.
Added commenting throughout for clarity during code review.

Makefile Changes

Makefile was updated and a Manifest.txt file created to ensure that the entry point for the application is the CashRegister.class. This is so that the CashRegister.jar file can be run using the command java -jar CashRegister.jar.