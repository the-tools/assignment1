Cash Register Application

Welcome to the Cash Register Application Wiki. Below you will find instructions for downloading and executing the application.
Authored By Matthew Morris, Scott Hanan, Rachael Shephard and Lachie Teague-Cornish.

Application Description

The Cash Register Application is a java based application designed to assist with Point Of Sale transaction. The application guides a register operator through a transaction with a step by step process on processing a customer's transaction. Below there are instructions on how to install and run the application as well as a description of the features including in the package.

Installing GitLab

To install GitLab, below is a link for instructions for use with many different operating systems including some Linux based systems. For systems such as Windows and MacOS, the most common way to install Gitlab is through a virtual machine running a version of linux such as ubuntu. Alternately for these systems, the files can be downloaded as a zip from the main page with no git integration.
GitLab installation

Downloading the Repo

Once Git is installed and a GitLab account is created, a user can download a repository by selecting the Blue Button Clone->Clone with HTTPS which will then copy the link that is necessary to download. Once that has been copied the user can open a terminal in the location where they want to store the local repository and write git clone . You now have access to a local repository.
As stated, the repo can also be downloaded through the download link on the main project page.

Installing Java

For Downloading Java, follow this link and select the appropriate file for your operating system.
Java Installation

Executing The Application

When executing the application, there are two possible routes.
The .jar file can be run through the java application. On Windows and MacOS this can be done by installing java and opening the .jar file with java. On Linux this can be done by installing the java package and running the command line with the command java -jar CashRegister.jar.
Alternately, once can use the command line with the command java CashRegister to run the .java file.
If code has been added to the java files, it will need to be complied before it can be executed with the new changes. To compile a java file this can be completed in the terminal when they are in the folder storing the files with command javac fileName.java. Once the code compiles with no errors, the user can then execute the file with java fileName.

Application Use and Functions

Basic Functionality:
When the program is first executed the user will be prompted to enter the cash register's float. The input must be an integer or decimal value. Once the float is entered the program then prompts the user to process a transaction (see Processing a transaction). When quitting the program the cash register's float will be displayed.
Multi Language Feature:
The Cash Register Application has the option to toggle between any predefined
language options. This toggle is configured by opening the CashRegister.java
and changing the langChoice to one of the variables that are listed as a
comment. Depending on what value to change it to, the programming will utilize
the corresponding language.
Coupon Code Feature:
The user has the option to add a discount to the total amount before the tender is given. The user is presented with 5 options to choose the discount required. Entering anything other than the given options will return an error and the user will be prompted to enter their choice again.
Processing a transaction:
When the user enters "p" after the prompt for "Would you like to process a transaction?" a transaction begins. The user is prompted for an item name and it's cost. After entering the item name and cost the user is then prompted if they would like to add anymore items. After the user has finished entering the items the user is asked if they have a coupon to apply (see Coupon Code Feature), then the user is prompted to enter the tender. The tender must be sufficient to cover the cost of the transaction before the option for a receipt is given. If the user chooses to get a receipt the items processed in the transaction, their cost, any discount, total, tender and change will all be displayed on the receipt.
Counter on Items in Receipt:
When the user accepts a receipt a new LinkedList will be created that calls the function createList which accepts the item list and costItem list. This function loops through those lists to check which items are the same and add a counter to it so that the receipt will be smaller and show values such as item x3 -$50.

Wiki Link - https://gitlab.com/the-tools/assignment1/wikis/Cash-Register-Application